Added mini goals based on current days steps (as an alternative to starting mini goal from zero)
Added localization (and French translation) - submitted by fabienli
Added import/export - submitted by fabienli

Bug fixes:
notification correctly shows user set target steps - submitted by fabienli
monthly step target based on number of days in month (rather than 30 days for all months)
Paseo_v1.2.2.apk